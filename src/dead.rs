fn str_white_at_pos(content: &str, pos: usize) -> bool { //TODO es gibt in rust anscheinend eine eingebaut .is_whitespace function
	if !content.is_char_boundary(pos) || !content.is_char_boundary(pos+1) {
		return false;
	}
	if &content[ pos .. pos+1  ] == " " || &content[ pos .. pos+1  ] == "\n" || &content[ pos .. pos+1	] == "\t" {
		return true
	}
	return false;
}
