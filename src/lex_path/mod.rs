use ::*;
pub struct Insertion {
	pub pos: usize,
	pub txt: String,
}
lazy_static! {
	//Stores the name of the current file being read. For Debug purposes only (GDV= Global Debug Variable)
	pub static ref GDV_FILE: std::sync::Mutex<std::string::String> = std::sync::Mutex::new(std::string::String::from("Just Started"));
	//https://users.rust-lang.org/t/how-can-i-use-mutable-lazy-static/3751 https://stackoverflow.com/questions/32555589/is-there-a-clean-way-to-have-a-global-mutable-state-in-a-rust-plugin
}
static DEBUG_PRINT_FILENAME: bool = false;
pub fn lex_path(path: &std::path::Path,
			write_to_file: bool,
			anal_opened_curl: Option<&Fn(anal_opened_curl_arguments)>,
			anal_function_body: Option<&Fn(anal_function_body_arguments)>,
			anal_closed_curl: Option<&Fn(anal_closed_curl_arguments)>,
			anal_command: Option<&Fn(anal_command_arguments)>
) {
	for entry in walkdir::WalkDir::new(&path) {
		let entry = entry.expect("weird error");
		if entry.file_type().is_file() {
			let extension = match entry.path().extension() {
				Some(ext) => ext.to_str().expect("Invalid UTF-8 in file extension"),
				None => "",
			};
			if extension == "C" || extension == "H" {
				if DEBUG_PRINT_FILENAME {
					println!("starting to read {:?}", entry.path());
				}
				//The following two lines set GDV_FILE to entry.path(). GDV_FILE can be read via println!("{:?}", *GDV_FILE.lock().unwrap());
				GDV_FILE.lock().unwrap().truncate(0);
				GDV_FILE.lock().unwrap().push_str(entry.path().to_str().expect(""));
				let source = std::fs::read_to_string(entry.path()).expect("unable to read file");
				//let source = source.replace(|c: char| !c.is_ascii(), ""); //#PERFORMANCE
				let source = &source; //equivalent to let source = source.as_str();
				let mut planned_insertions = Vec::<Insertion>::new();
				let mut state_curled_braces = Vec::<&str>::new();
				let mut state_do_while = false;
				let mut state_file_macros = Vec::<std::string::String>::new();
				let mut pos = 0;
				while pos < source.len() {
					lex_step(source,
					         &mut pos,
					         entry.path().to_str().expect("weird error"),
					         &mut state_curled_braces,
					         &mut state_file_macros,
					         &mut state_do_while,
					         &mut planned_insertions,
					         anal_opened_curl,
					         anal_function_body,
					         anal_closed_curl,
					         anal_command);
				}
				assert!(state_do_while == false, "state_do_while is not false. File: {:?}", entry.path());
				assert!(state_curled_braces.len() == 0, "state_curled_braces is not empty: {:?} {:?}", state_curled_braces, entry.path());
				if DEBUG_PRINT_FILENAME {
					println!("finished reading {:?}", entry.path());
				}
				planned_insertions.sort_by(|a,b| a.pos.cmp(&b.pos));
				if planned_insertions.len() > 0 {// wenn man diese if-schleife weg macht ändert er das Änderungsdatum von allen Dateien, nicht nur von den bei denen man es braucht
					if write_to_file {
						/*println!("Are you sure, that you want to write into {:?} ? Enter y for confirmation", entry.path());
						let mut buffer = String::new();
						std::io::stdin().read_line(&mut buffer).expect("unable to read from stdin");
						println!("{}", buffer);
						if buffer == "y\n"  */
						{
							println!("writing to {:?}", entry.path());
							planned_insertions.push(Insertion{pos:source.len(),txt:"".to_string() });
							let mut outfile_src = std::fs::File::create(entry.path()).expect("unable to create file");
							outfile_src.write(source[0..planned_insertions[0].pos].as_bytes()).expect("unable to write");
							for i in 0..planned_insertions.len()-1 {
								outfile_src.write(planned_insertions[i].txt.as_bytes()).expect("unable to write"); //TODO: es sollte nicht planned_insertions geschrieben werden, sondern /*BEGIN_LEXA*/palnned_insertions/*END_LEXA*/\n
								outfile_src.write(source[planned_insertions[i].pos..planned_insertions[i+1].pos].as_bytes()).expect("unable to write");
							}
						} /*else {
							println!("not writing");
						}*/
					} else {
						println!("planned_insertions for file {:?}:", entry.path());
						for ins in planned_insertions {
							//println!("\tline: {} txt: {}", pos_to_line(&source, ins.pos), &(ins.txt));
							//if ins.pos != 0 {
							//if ins.txt == HOOK_BEGIN_RUNPOINT { //das ist dirty. 
								println!("\t line:{}", pos_to_line(&source, ins.pos));
							//}
						}
					}
				}
			}
		}
	}
}
