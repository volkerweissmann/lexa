use ::helper::*;
use ::stranal::*;
use ::*;
static KNOWN_MACROS: &'static [&str] = &["makeFvDdtScheme", //das hier soll nicht mehr benötigt werden, statdessen soll man das aus dem stil herauslesen
										 "makeFvD2dt2Scheme",
										 "makeSnGradScheme",
										 "makeSnGradScheme",
										 "makeFvConvectionScheme",
										 "makeMultivariateFvConvectionScheme",
										 "makeFvDivScheme",
										 "makeFvLimitedGradTypeScheme",
										 "makeNamedFvLimitedGradTypeScheme",
										 "makeFvLimitedGradScheme",
										 "makeFvGradScheme",
										 "makeLeastSquaresGradScheme",
                                         "makeFvLaplacianScheme",
										 "PRODUCT_OPERATOR",
                                         "BINARY_FUNCTION",
                                         "BINARY_TYPE_FUNCTION",
                                         "UNARY_OPERATOR",
										 "BINARY_OPERATOR",
                                         "BINARY_TYPE_OPERATOR",
                                         "UNARY_FUNCTION",
                                         "BINARY_TYPE_OPERATOR_SF",
                                         "BINARY_TYPE_OPERATOR_FS"];
fn known_macro_at_pos<'a>(source: &str, pos: usize, file_macros: &'a Vec<std::string::String>) -> Option<&'a str> {
	for m in KNOWN_MACROS {
		if str_at_pos(source, pos, m) {
			return Some(m);
		}
	}
	for m in file_macros {
		if str_at_pos(source, pos, m) {
			return Some(m);
		}
	}
	return None;
}
pub fn expect_and_jump_braces(source: &str, posp: &mut usize, opening: &str, closing: &str) { //#PERFORMANCE: ist char schneller als &str
	let anal_closure = |src: &str, pos: usize, state: &mut (i32,bool)| -> bool { //10x: 6,52 s 1x: 2,7
		if str_at_pos_half_check(src, pos, opening) {
			state.0 += 1;
			state.1 = true;
		} else if str_at_pos_half_check(src, pos, closing) {
			assert!(state.0 != 0);
			state.0 -= 1;
			if state.0 == 0 {
				return true;
			}
		}
		return false;
	};
	let mut state = (0,false);
	walk_normal_str_with_closure(source, posp, anal_closure, &mut state);//#BEST_HELPER
	assert!(state.1);
	*posp += 1;
}
pub fn check_and_jump_hashtag_command_otwc(source: &str, posp: &mut usize, file_macros: &mut Vec<std::string::String>) -> bool {
	if !str_at_pos(&source, *posp, "#") {
		return false;
	}
	if DEBUG_PRINTS >= 10 {
		println!("\tFound a pre-processing directive");
	}
	let anal_closure = |src: &str, pos: &mut usize, char_state: i32, state: &mut (i32, usize, usize) | -> bool {
		if (char_state == STATE_NORMAL || char_state == STATE_COMMENT_SINGLE_LINE)
			&&str_at_pos(&source, *pos, "\n") && !str_at_pos(&source, *pos-1, "\\") {
			return true;
		}
		if char_state == STATE_NORMAL {
			if state.0 == 0 {
				if stranal::str_at_pos(src, *pos, "define") {
					*pos += "define".len();
					state.0 = 1;
					state.1 = *pos;
				}
			} else if state.0 == 1 {
				if !str_at_pos_first_match(src, *pos, &[" ", "\t" , "\n"]).is_none() { //wenn es ein whitespace zeichen ist
					state.1 = *pos;
				} else {
					state.1 = *pos;
					state.0 = 2;
					state.2 = *pos;
				}

			} else if state.0 == 2 {
				if str_at_pos_first_match(src, *pos, &[" ", "\t" , "\n", "("]).is_none() {
					state.2 = *pos;
				} else {
					state.2 = *pos;
					state.0 = 3;
				}
			}
		} else if char_state == STATE_COMMENT_SINGLE_LINE || char_state == STATE_COMMENT_SINGLE_LINE {
			if state.0 == 1 {
				state.1 = *pos;
				state.0 = 2;
				state.2 = *pos;
			} else if state.0 == 2 {
				state.2 = *pos;
				state.0 = 3;
			}
		}
		return false;
	}; //TODO STYLE: das muss man doch auch in weniger zeilen hinkriegen, z.B. ein split_at(source, " ", "\n", "\t", "//", "/*", "(")
	let mut state = (0, 0, 0);
	if !walk_state_str_with_closure(&source, posp, anal_closure, &mut state ) {
		panic!("unable to process pre-processing command");
	}
	if state.0 == 3 {
		file_macros.push(std::string::String::from(&source[state.1..state.2]));
	}
	/*if let Some(s) = copy_until_normal(&source, pos, &["(", "\\", " ", "\t" , "\n"]) {
	file_macros.push(std::string::String::from(s));
} else {
	panic!("weird macro");
}*/
	/*println!("{:?}", file_macros);
	//while !(str_at_pos(&source, pos, "\n") && !str_at_pos(&source, pos-1, "\\") ) {
	while !str_at_pos(&source, pos, "\n") || str_at_pos(&source, pos-1, "\\") {
	pos += 1;
}
	pos += 1;*/
	if DEBUG_PRINTS >= 10 {
		print_state(&source, *posp, 1, "after pre-processing command");
	}
	return true;
}
pub fn check_and_jump_known_macro_otwc(source: &str, posp: &mut usize, filepath: &str, state_file_macros: &mut Vec<std::string::String>) -> bool {
	if let Some(m) = known_macro_at_pos(&source, *posp, &state_file_macros) {//https://doc.rust-lang.org/rust-by-example/flow_control/if_let.html
		if DEBUG_PRINTS >= 10 { 
			println!("\tFound a known macro: {}", m);
		}
		*posp += m.len();
		if !jump_to_next_normal_slice_with_cond(&source, posp, 1, &|s| s == "(" || s == ";") {
			print_state(&source, *posp, 0, "unable to find ( or ;");
			println!("Problem in file (makro-type): {}", filepath);
			if DEBUG_PANIC_ON_PROBLEM {
				panic!("Parsing Problem");
			}
			return true;
		}
		if str_at_pos(&source, *posp, ";") {
			*posp += 1;
			if DEBUG_PRINTS >= 10 {
				print_state(&source, *posp, 1, "after macro");
			}
			return true;
		}
		*posp += 1;
		let mut round_braces_depth = 1;
		while round_braces_depth != 0 {
			*posp += 1;
			if !jump_to_next_normal_slice_with_cond(&source, posp, 1, &|s| s == "(" || s == ")") {
				print_state(&source, *posp, 0, "unable to find )");
				println!("Problem in file (makro-type): {}", filepath);
				if DEBUG_PANIC_ON_PROBLEM {
					panic!("Parsing Problem");
				}
				return true;
			}
			//println!("FOUND A CHAR: {}", &source[pos..pos+1]);
			if &source[*posp..*posp+1] == ")" {
				round_braces_depth -= 1;
				if DEBUG_PRINTS >= 20 {
					print_state(&source, *posp, 2, &format!("found a ), new depth = {}", round_braces_depth));
				}
			} else if &source[*posp..*posp+1] == "(" {
				round_braces_depth += 1;
				if DEBUG_PRINTS >= 20 {
					print_state(&source, *posp, 2, &format!("found a (, new depth = {}", round_braces_depth));
				}
			}
		}
		*posp += 1;
		if DEBUG_PRINTS >= 10 {
			print_state(&source, *posp, 1, "after macro");
		}
		return true;
	}
	return false;
}
//jump_over_template_if_there
//   no leading whitespace or leading comment is allowed
//   jumps over template and whitespace and commment after template
pub fn check_and_jump_template_otwc(source: &str, pos: &mut usize) {
	let anal_closure = |src: &str, pos: usize, brace_count: &mut i32| -> bool{
		if *brace_count == -1 {
			return true;
		}
		if ::stranal::str_at_pos(src, pos, "<") {
			*brace_count += 1;
		} else if ::stranal::str_at_pos(src, pos, ">") {
			*brace_count -= 1;
			if *brace_count == 0 {
				*brace_count = -1;
			}
		}
		return false;
	};
	loop {
	let mut brace_count = 0;
		if ::stranal::str_at_pos(source, *pos, "template") {
			*pos += "template".len();
			walk_normal_str_with_closure(source, pos, anal_closure, &mut brace_count);
			check_and_jump_comment_and_whitespace(source, pos);
		} else {
			return;
		}
	}
}
pub enum ReasonCurledBrace <'a> {
	Namespace,
	Enum,
	ArrayDef,
	ArrayArg,
	Class (&'a str),
	Struct,
	Function,
	If,
	ElseIf,
	Else,
	Extern,
	SwitchCaseSwitch,
	SwitchCaseCase,
	SwitchCaseDefault,
	Union,
	Optics,
	Unknown,
}
pub static OBJ_NAMESPACE: i32 = 1;
pub static OBJ_ENUM: i32 = 2;
pub static OBJ_ARRAY_DEF: i32 = 3;
pub static OBJ_ARRAY_ARG: i32 = 4;
pub static OBJ_CLASS: i32 = 5;
pub static OBJ_STRUCT: i32 = 6;
pub static OBJ_FUNCTION: i32 = 7;
pub static OBJ_UNKNOWN: i32 = 8;
pub static OBJ_IF: i32 = 9;
pub static OBJ_ELSE_IF: i32 = 10;
pub static OBJ_ELSE: i32 = 11;
pub static OBJ_EXTERN: i32 = 12;
pub static OBJ_OPTICS: i32 = 13;
pub static OBJ_SWITCH_CASE_SWITCH: i32 = 14;
pub static OBJ_SWITCH_CASE_CASE: i32 = 15;
pub static OBJ_SWITCH_CASE_DEFAULT: i32 = 16;
pub static OBJ_UNION: i32 = 17;

static DEBUG_REASON_FOR_CURLED_BRACE: bool = false;
///reason_for_curled_brace:
///no leading whitespace or leading comment is allowed
pub fn reason_for_curled_brace(part: &str) -> ReasonCurledBrace {
	if DEBUG_REASON_FOR_CURLED_BRACE {
		println!("reason_for_curled_brace: {}", &part.replace("\n", " "));
	}
	if part == "" {
		return ReasonCurledBrace::Optics;
	}
    if str_at_pos(part, 0, ",") {
        return ReasonCurledBrace::ArrayArg;
    }
	let mut pos = 0;
	if !jump_to(part, &mut pos, &[" ", ":", "=", "(", "{", " ", "\t", "\n", "//", "/*",]) {
		println!("warning: unknown reason for a curled brace: {}", &part.replace("\n", " "));
		return ReasonCurledBrace::Unknown;
	}
	if &part[0..pos] == "namespace" {
		return ReasonCurledBrace::Namespace;
	}
	if &part[0..pos] == "enum" {
		return ReasonCurledBrace::Enum;
	}
	if &part[0..pos] == "if" {
		return ReasonCurledBrace::If;
	}
	if &part[0..pos] == "else" {
		pos += 1;
		if !jump_to(part, &mut pos, &[" ", ":", "=", "{", " ", "\t", "\n", "//", "/*",]) {
			return ReasonCurledBrace::Else;
		}
		if &part[0..pos] == "else if"{
			return ReasonCurledBrace::ElseIf;
		}
		return ReasonCurledBrace::Else;
	}
	if &part[0..pos] == "extern" {
		return ReasonCurledBrace::Extern;
	}
	if &part[0..pos] == "switch" {
		return ReasonCurledBrace::SwitchCaseSwitch;
	}
	if &part[0..pos] == "case" {
		return ReasonCurledBrace::SwitchCaseCase;
	}
	if &part[0..pos] == "default" {
		return ReasonCurledBrace::SwitchCaseDefault;
	}
	if &part[0..pos] == "union" {
		return ReasonCurledBrace::Union;
	}
	pos = 0;
	//::print_state(source, pos, 0, "vor jump over template");
	check_and_jump_template_otwc(part, &mut pos);
	//::print_state(source, pos, 0, "nach jump over template");
	let mut tpos = pos;
	jump_to_normal(part, &mut tpos, &["=", "("]);
	if str_at_pos(part, tpos, "=") {
		return ReasonCurledBrace::ArrayDef;
	}
	let before_class = pos;
	if !jump_to(part, &mut pos, &[" ", "\t", "\n", "//", "/*"]) {
		return ReasonCurledBrace::Unknown;
	}
	if &part[before_class..pos] == "class" {
		check_and_jump_comment_and_whitespace(part, &mut pos);
		let before_name = pos;
		jump_to(part, &mut pos, &[":", " ", "\t", "\n", "//", "/*"]);
        let classname = &part[before_name..pos];
        check_and_jump_comment_and_whitespace(part, &mut pos);
        let mut base_classes = Vec::<&str>::new();
        if pos != part.len() { //Wenn wir eine Klasse haben die von einer anderen Klasse erbt
        }
        return ReasonCurledBrace::Class(classname);
        //if &part[pos..pos+1] != ""
        //let after_name = pos;
        //while jump_to(part, &mut pos, &[","]);
	}
	if &part[before_class..pos] == "struct" {
		return ReasonCurledBrace::Struct;
	}
	
	return ReasonCurledBrace::Function;
}
static PRINT_ANAL_FUNCTION_SIGNATURE: bool = false;
static PAUSE_ANAL_FUNCTION_SIGNATURE: bool = false;
pub fn anal_function_signature(source: &str) {//TODO diese Funktion soll obsolet werden, man beachte, dass es reason_for_curled_brace gibt. //TODO: a< b > func(){} ist eine valide funktion, würde aber hier probleme machen, denn a< b > ist zwar ein datentyp, hat aber leerzeichen
	if PRINT_ANAL_FUNCTION_SIGNATURE {
		println!("#################################### source = ");
		println!("{}", source);
	}
	let mut pos = 0;
	check_and_jump_template_otwc(source, &mut pos);
	let mut pos_before_jump = pos;
	let mut _function_name: &str = "";
	/*let mut datatype_of_return: &str = "";
	let mut const_return = false;
	let mut inline_func = false;
	let mut virtual_func = false;
	let mut depends_typename = false;*/
	loop {
		if PRINT_ANAL_FUNCTION_SIGNATURE {
			::print_state(source, pos, 0, "loop start:");
		}
		if !::stranal::jump_to(source, &mut pos, &[" ", "\t", "\n", "//", "/*", "("]) {
			panic!("unable to parse function signature: file: {} source: {}", *GDV_FILE.lock().unwrap(), source);
		}
		if PRINT_ANAL_FUNCTION_SIGNATURE {
			::print_state(source, pos, 0, "after jump");
			println!("next char is {}, we found a {}", &source[pos..pos+1] , &source[pos_before_jump..pos]);
		}
		_function_name = &source[pos_before_jump..pos];
		/*if &source[pos_before_jump..pos] == "const" {
			const_return = true;
		} else if &source[pos_before_jump..pos] == "inline" {
			inline_func = true;
		} else if &source[pos_before_jump..pos] == "virtual" {
			virtual_func = true;
		} else if &source[pos_before_jump..pos] == "typename" {
			depends_typename = true;
		} else {
			function_name = &source[pos_before_jump..pos];
			jump_over_comment_and_whitespace(source, &mut pos);
			if &source[pos..pos+1] == "(" { //In diese if-Schleife geht es immer dann rein, wenn es ein konstruktor oder ein destructor ist.
				break;
			}
			pos_before_jump = pos;
			if !::stranal::jump_to(source, &mut pos, &[" ", "\t", "\n", "//", "/*", "("]) { */
				panic!("unable to parse function signature");
			}
			datatype_of_return = function_name;
			function_name = &source[pos_before_jump..pos];
			jump_over_comment_and_whitespace(source, &mut pos);
			if &source[pos..pos+1] == "(" {
				break;
			} else {
				println!("=========================");
				println!("{}", source);
				panic!("unable to parse function signature");
			}
		}*//*else if function_name == "" && &source[pos_before_jump..pos] != "" {
			function_name = &source[pos_before_jump..pos];
			if DEBUG_ANAL_FUNCTION_SIGNATURE {
				println!("new function_name = {}", function_name);
			}
		} else if datatype_of_return == "" {
			datatype_of_return = function_name;
			function_name = &source[pos_before_jump..pos];
			if DEBUG_ANAL_FUNCTION_SIGNATURE {
				println!("new function_name = {}, new datatype_of_return = {}", function_name, datatype_of_return);
			}
		} else if &source[pos..pos+1] == "(" {
			break;
		} else {
			panic!("unable to parse function signature");
		}*/
		check_and_jump_comment_and_whitespace(source, &mut pos);
		if ::stranal::str_at_pos(source, pos, "(") {
			break;
		}
		pos_before_jump = pos;
	}
	if PAUSE_ANAL_FUNCTION_SIGNATURE {
		::pause();
	}
}
