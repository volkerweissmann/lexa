pub fn incr_char_boundary(source: &str, pos: &mut usize) -> bool {
	*pos += 1;
	if *pos >= source.len() {
		return false;
	}
	while !source.is_char_boundary(*pos) {
		*pos += 1;
		if *pos >= source.len() {
			return false;
		}
	}
	return true;
}
pub fn str_at_pos(source: &str, pos: usize, search: &str) -> bool {
	if !source.is_char_boundary(pos) || !source.is_char_boundary(pos+search.len()) {
		return false;
	}
	if &source[ pos .. pos+search.len()	 ] == search {
		return true;
	}
	return false;
}
pub fn str_at_pos_half_check(source: &str, pos: usize, search: &str) -> bool {
	if !source.is_char_boundary(pos+search.len()) {
		return false;
	}
	if &source[ pos .. pos+search.len()	 ] == search {
		return true;
	}
	return false;
}
pub fn slice_until_delim_or_eof<'a>(source: &'a str, start: usize, delim: &'static [&str]) -> &'a str {
	let mut pos = start;
	loop {
		for d in delim {
			if str_at_pos_half_check(source, pos, d) {
				return &source[start..pos];
			}
		}
		if !incr_char_boundary(source, &mut pos) {
			return &source[start..];
		}
	}
}
pub fn str_at_pos_first_match(source: &str, pos: usize, search: &'static [&str]) -> Option<usize> {    
	if !source.is_char_boundary(pos) {
		return None;
	}
	for i in 0..search.len() {
		if source.is_char_boundary(pos+search[i].len()) && &source[ pos .. pos+search[i].len()	 ] == search[i] {
			return Some(i);
		}
	}
	return None;
}
/*pub fn var2_anal_str_with_closure<F, R>(source: &str, mut closure: F, start_state: R) -> R where F: FnMut(&str,usize, R) -> R {
	let mut state = start_state;
	//R = closure(source, 0, start_state);
	for i in 0..source.len() {
		state = closure(source, i, state);
	}
	return state;
}*/
pub fn anal_str_with_closure<F, R>(source: &str, mut closure: F, state: &mut R) where F: FnMut(&str,usize, &mut R) { //TODO geht auch:
//pub fn anal_str_with_closure<F, R>(source: &str, closure: F, state: &mut R) where F: Fn(&str,usize, &mut R) {
	for i in 0..source.len() {
		closure(source, i, state);
	}
}
pub fn walk_str_with_closure<F, R>(source: &str, pos: &mut usize, mut closure: F, state: &mut R) -> bool where F: FnMut(&str,usize, &mut R) -> bool {
	loop {
		if closure(source, *pos, state) {
			return true;
		}
		*pos += 1;
		if *pos >= source.len() {
			return false;
		}
	}
}
pub fn jump_to(source: &str, pos: &mut usize, search: &'static [&str]) -> bool {
	loop {
		for s in search {
			if str_at_pos(source, *pos, s) { //PERFORMANCE: source.is_char_boundary(*pos) ist definitiv erfüllt und muss hier nicht nochmal überprüft werden
				return true;
			}
		}
		*pos += 1;
		if *pos >= source.len() {
			return false;
		}
		while !source.is_char_boundary(*pos) {
			*pos += 1;
			if *pos >= source.len() {
				return false;
			}
		}
	}
}
