pub static STATE_NORMAL: i32 = 0;
pub static STATE_COMMENT_SINGLE_LINE: i32 = 1;
pub static STATE_COMMENT_MULTI_LINE: i32 = 2;
pub static STATE_CHAR_FRESH: i32 = 3;
pub static STATE_CHAR_OLD: i32 = 5;
pub static STATE_STRING_FRESH: i32 = 4;
pub static STATE_STRING_OLD: i32 = 6;
fn comment_and_str_state_changes(source: &str, pos: &mut usize, state: &mut i32) { //TODO pos sollte usize sein und nicht &mut usize
    //PERFORMANCE wenn er außerhalb eines Kommentares ist soll *state == STATE... nicht überprüft werden, die Information über state soll durch den Program Counter gegeben sein.
	if *state == STATE_COMMENT_SINGLE_LINE && source.is_char_boundary(*pos-1) && &source[*pos-1..*pos] == "\n"{
		*state = STATE_NORMAL;
	} else if *state == STATE_COMMENT_MULTI_LINE && *pos >= 2 && source.is_char_boundary(*pos-2) && &source[*pos-2..*pos] == "*/"{
		*state = STATE_NORMAL;
	} else if *state == STATE_CHAR_OLD && source.is_char_boundary(*pos-1) && &source[*pos-1..*pos] == "'" {
		let mut even_num_backslash = true;
		let mut temp_pos = *pos-1;
		while source.is_char_boundary(temp_pos-1) && &source[temp_pos-1..temp_pos] == "\\" {
			temp_pos -= 1;
			even_num_backslash = !even_num_backslash;
		}
		if even_num_backslash {
			*state = STATE_NORMAL;
		}
	} else if *state == STATE_STRING_OLD && source.is_char_boundary(*pos-1) && &source[*pos-1..*pos] == "\"" {
		let mut even_num_backslash = true;
		let mut temp_pos = *pos-1;
		while source.is_char_boundary(temp_pos-1) && &source[temp_pos-1..temp_pos] == "\\" {
			temp_pos -= 1;
			even_num_backslash = !even_num_backslash;
		}
		if even_num_backslash {
			*state = STATE_NORMAL;
		}
	}
	if *state == STATE_NORMAL && source.is_char_boundary(*pos+2) && &source[*pos..*pos+2] == "//" { //#PERFORMANCE sollte hier evtl. else if statt if stehen?
		*state = STATE_COMMENT_SINGLE_LINE;
	} else if *state == STATE_NORMAL && source.is_char_boundary(*pos+2) && &source[*pos..*pos+2] == "/*" {
		*state = STATE_COMMENT_MULTI_LINE;
	} else if *state == STATE_NORMAL && source.is_char_boundary(*pos+1) && &source[*pos..*pos+1] == "'" {
		*state = STATE_CHAR_FRESH;
	} else if *state == STATE_NORMAL && source.is_char_boundary(*pos+1) && &source[*pos..*pos+1] == "\"" {
		*state = STATE_STRING_FRESH;
	} else if *state == STATE_CHAR_FRESH {
		*state = STATE_CHAR_OLD;
	} else if *state == STATE_STRING_FRESH {
		*state = STATE_STRING_OLD;
	}
}
pub fn check_and_jump_comment_and_whitespace(source: &str, pos: &mut usize) -> bool {
	let start_pos = *pos;
	loop {
		if ::stranal::str_at_pos(&source, *pos, "/*") { //Mehrzeilige Kommentare überspringen
			//pos += 2; #PERFORMANCE gibt das bessere performance
			while !::stranal::str_at_pos(&source, *pos, "*/") {
				*pos += 1;
			}
			*pos += 2;
		} else if ::stranal::str_at_pos(&source, *pos, "//") {//Einzeilige Kommentare überspringen
			//pos += 2 #PERFORMANCE gibt das bessere performance
			while !::stranal::str_at_pos(&source, *pos, "\n") || ::stranal::str_at_pos(&source, *pos-1, "\\") {
				*pos += 1;
			}
			*pos += 1;
		} else if ::stranal::str_at_pos(&source, *pos, " ") || ::stranal::str_at_pos(&source, *pos, "\t") || ::stranal::str_at_pos(&source, *pos, "\n"){
			*pos += 1; //#PERFORMANCE #STYLE evtl. incr_char_boundary und str_at_pos_half_check überprüfen
		} else {
			return *pos != start_pos;
		}
		if *pos >= source.len() {
			return *pos != start_pos;
		}
	}
	/*let mut state = 0;
	loop {
		comment_and_str_state_changes(source, pos, &mut state);
		if state == 0 && source.is_char_boundary(*pos+1) {
			if &source[*pos..*pos+1] != " " && &source[*pos..*pos+1] != "\t" && &source[*pos..*pos+1] != "\n" {
				return;
			}
		}
		*pos += 1;
		if *pos >= source.len() {
			return;
		}
		while !source.is_char_boundary(*pos) {
			*pos += 1;
			if *pos >= source.len() {
				return;
			}
		}
}*/
}
static DEBUG_JTNNSWC: bool = false; 
pub fn jump_to_next_normal_slice_with_cond(source: &str, pos: &mut usize, length: usize, cond: &Fn(&str) -> bool) -> bool { //normal heißt, es darf kein kommentar sein und kein string doer char
	//TODO ich hoffe, das diese funktion eines tages obsolet wird
	let mut state = STATE_NORMAL; //0: normal, 1: single-line-comment, 2: multi-line-comment, 3: char, 4: string, 5: char seit einem zeichen, 6: string seit einem zeichen
	loop {
		if DEBUG_JTNNSWC {
			println!("Begin: state: {} char: {}", state, &source[*pos..*pos+1]);
		}
		comment_and_str_state_changes(source, pos, &mut state); //TODO: anstelle von commend_and_str_changees ist evtl. jump_over_comment_and_whitespace schneller
		if DEBUG_JTNNSWC {
			println!("After state changes: state: {} char: {}", state, &source[*pos..*pos+1]);
		}
		if state == STATE_NORMAL && source.is_char_boundary(*pos+length) {
			if DEBUG_JTNNSWC {
				println!("{}", (&source[*pos..*pos+length]).replace("\n","_"));
			}
			if cond(&source[*pos..*pos+length]) {
				return true;
			}
		}
		*pos += 1;
		if *pos >= source.len() {
			return false;
		}
		while !source.is_char_boundary(*pos) {
			*pos += 1;
			if *pos >= source.len() {
				return false;
			}
		}
	}
}
pub fn jump_to_normal(source: &str, opos: &mut usize, search: &'static [&str]) -> bool {
	let anal_closure = |src: &str, pos: usize, _unvar: &mut ()| -> bool {
		for s in search {
			if ::stranal::str_at_pos(src, pos, s) { //PERFORMANCE: source.is_char_boundary(*pos) ist definitiv erfüllt und muss hier nicht nochmal überprüft werden
				return true;
			}
		}
		return false;
	};
	return walk_normal_str_with_closure(source, &mut *opos, anal_closure, &mut ()); //TODO muss hier &mut *opos oder opos hin
}
/*pub fn jump_to_normal_whats_first_plus_one(source: &str, pos: &mut usize, search: &'static [&str]) -> usize { //Funktioniert, ist aber auskommentiert da es toter code ist
	loop {
		for i in 0..search.len() {
			if ::stranal::str_at_pos(src, *pos, s[i]) { //PERFORMANCE: source.is_char_boundary(*pos) ist definitiv erfüllt und muss hier nicht nochmal überprüft werden
				return i+1;
			}
		}
		*pos += 1;
		if *pos >= source.len() {
			return 0;
		}
		while !source.is_char_boundary(*pos) {
			*pos += 1;
			if *pos >= source.len() {
				return 0;
			}
		}
	}
}*/
pub fn copy_until_normal<'a>(source: &'a str, start_pos: usize, search: &'static [&str]) -> Option<&'a str> {
	let mut end_pos = start_pos;
	if jump_to_normal(source, &mut end_pos, search) {
		return Some(&source[start_pos..end_pos]);
	} else {
		return None;
	}
}
pub fn jump_and_copy_until_normal<'a>(source: &'a str, pos: &mut usize, search: &'static [&str]) -> Option<&'a str> {
	let start_pos = *pos;
	if jump_to_normal(source, pos, search) {
		return Some(&source[start_pos..*pos]);
	} else {
		return None;
	}
}
pub fn walk_normal_str_with_closure<F, R>(source: &str, pos: &mut usize, mut closure: F, closure_state: &mut R) -> bool where F: FnMut(&str,usize, &mut R) -> bool {
	let mut state = STATE_NORMAL; //0: normal, 1: single-line-comment, 2: multi-line-comment, 3: char, 4: string, 5: char seit einem zeichen, 6: string seit einem zeichen
	loop {
		comment_and_str_state_changes(source, pos, &mut state);//TODO: anstelle von commend_and_str_changees ist evtl. jump_over_comment_and_whitespace schneller
		if state == STATE_NORMAL {
			if closure(source, *pos, closure_state) {
				return true;
			}
		}
		*pos += 1;
		if *pos >= source.len() {
			return false;
		}
		while !source.is_char_boundary(*pos) {
			*pos += 1;
			if *pos >= source.len() {
				return false;
			}
		}
	}
}
pub fn walk_state_str_with_closure<F, R>(source: &str, pos: &mut usize, mut closure: F, closure_state: &mut R) -> bool where F: FnMut(&str,&mut usize,i32, &mut R) -> bool {
	let mut state: i32 = STATE_NORMAL; //0: normal, 1: single-line-comment, 2: multi-line-comment, 3: char, 4: string, 5: char seit einem zeichen, 6: string seit einem zeichen
	loop {
		comment_and_str_state_changes(source, pos, &mut state);
		if closure(source, pos, state, closure_state) {
			return true;
		}
		*pos += 1;
		if *pos >= source.len() {
			return false;
		}
		while !source.is_char_boundary(*pos) {
			*pos += 1;
			if *pos >= source.len() {
				return false;
			}
		}
	}
}
