use ::*;
pub fn remove_hooks(path: &std::path::Path) {	
	for entry in walkdir::WalkDir::new(&path) {
		let entry = entry.expect("weird error");
		if entry.file_type().is_file() {
			let extension = match entry.path().extension() {
				Some(ext) => ext.to_str().expect("weird error"),
				None => "",
			};
			if extension == "C" || extension == "H" {
				let old_content = std::fs::read_to_string(entry.path()).expect("unable to read file");
				let re = regex::Regex::new(r"/\*BEGIN_LEXA\*/(?s).*?/\*END_LEXA\*/\n").unwrap();//https://docs.rs/regex/1.0.5/regex/
				if re.is_match(&old_content) {
					let new_content = re.replace_all(&old_content, "");
					let mut outfile = std::fs::File::create(entry.path()).expect("unable to create file");
					outfile.write(new_content.as_bytes()).expect("unable to write to file");
				}
			}
		}
	}
}
pub fn pause() {
	let mut stdin = std::io::stdin();
	let mut stdout = std::io::stdout();
	// We want the cursor to stay at the end of the line, so we print without a newline and flush manually.
	//write!(stdout, "Press any key to continue...").unwrap();
	write!(stdout, "...").unwrap();
	stdout.flush().unwrap();
	// Read a single byte and discard
	let _ = stdin.read(&mut [0u8]).unwrap();
}
pub fn pos_to_line(source: &str, pos: usize) -> usize { //PERFORMANCE: https://crates.io/crates/bytecount https://www.reddit.com/r/rust/comments/5gj774/go_user_learning_rust_is_there_an_efficient_way/
	let mut line = 1;
	for i in 0..pos {
		if str_at_pos(source, i, "\n") {
			line += 1;
		}
	}
	line
}
pub fn print_state(source: &str, pos: usize, lvl: usize, msg: &str) {
	println!("{}{}", "\t".repeat(lvl), msg);
	println!("{}prev = |{}| next=|{}| line = {}",
			 "\t".repeat(lvl),
			 //&source[std::cmp::max(0, pos as i64 -10	 ) as usize..pos].replace("\n", &("\n".to_owned()+&("\t".repeat(lvl))) ),
			 &source[std::cmp::max(0, pos as i64 -10   ) as usize..std::cmp::min(source.len(),pos)].replace("\n", "_"),
			 &source[std::cmp::min(source.len()-1,pos)..std::cmp::min(source.len(),pos+10)].replace("\n", "_"),
			 pos_to_line(source, pos)
	);
}

static HOOK_INCLUDES_FOR_RUNPOINTS: &'static str = "/*BEGIN_LEXA*/#include<fstream>\n#include<chrono>/*END_LEXA*/\n";
pub static HOOK_BEGIN_RUNPOINT: &'static str = "/*BEGIN_LEXA*/{std::ofstream LEXA_OFS(\"/opt/HOOK_OUT.txt\",std::ofstream::out|std::ofstream::app);LEXA_OFS<<std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()<<' '<<__FILE__<<' '<<__func__<<' '<<__LINE__<<\": ";
static HOOK_END_RUNPOINT: &'static str = "\" << '\\n';}/*END_LEXA*/\n";
//TODO lifetimes auf die reihe kreigen, planned_insertions soll die ownership der Strings haben, add_runpoint soll Anführungszeichen in str entfernen
/*fn add_runpoint(planned_insertions: &mut Vec<Insertion>, pos: usize, comment: &'static str) { muss an die änderung von &str an String in Insertion angepasst werden
	planned_insertions.push(Insertion {pos: 0, txt: &HOOK_INCLUDES_FOR_RUNPOINTS});
	planned_insertions.push(Insertion {pos: pos, txt: &HOOK_BEGIN_RUNPOINT});
	planned_insertions.push(Insertion {pos: pos, txt: &comment});
	planned_insertions.push(Insertion {pos: pos, txt: &HOOK_END_RUNPOINT});
}*/
