use ::misc::*;
use ::helper::*;
use ::stranal::*;
use ::*;
pub struct anal_opened_curl_arguments<'a: 'b, 'b> {
	pub source: &'a str,
	pub start_pos: usize,
	pub end_pos: usize,
	pub file: &'a str,
	pub reason: ReasonCurledBrace<'a>,
	pub planned_insertions: &'b mut Vec<::Insertion>,
}
pub struct anal_function_body_arguments<'a: 'b, 'b> {
	pub source: &'a str,
	pub start_pos: usize,
	pub middle_pos: usize,
	pub end_pos: usize,
	pub file: &'a str,
	pub planned_insertions: &'b mut Vec<::Insertion>,
}
pub struct anal_closed_curl_arguments<'a: 'b, 'b> {
	pub source: &'a str,
	pub pos: usize,
	pub state_curled_braces: &'b mut Vec<&'a str>,
}
pub struct anal_command_arguments<'a: 'b, 'b> {
	pub source: &'a str,
	pub start_pos: usize,
	pub end_pos: usize,
	pub state_curled_braces: &'b Vec<&'a str>,
	pub file: &'a str,
	pub planned_insertions: &'a mut Vec<::Insertion>,
}
pub static DEBUG_PANIC_ON_PROBLEM: bool = true;
static DEBUG_PRINT_CURLED: bool = false;
pub static DEBUG_PRINTS: i32 = 0;
static DEBUG_PAUSES: i32 = 0;
static DEBUG_PAUSE_AFTER_LINE: usize = 0;
//Hinweis: Es ist die Aufgabe von lex_step pos jeden Durchlauf zu inkrementieren, nicht die Aufgabe von lex_path
pub fn lex_step<'a, 'b>(source: &'a str,
					posp: &mut usize,
					filepath: &str,
					state_curled_braces: &'b mut Vec<&'a str>, //das erste 'a ist neu und vielleicht falsch
					state_file_macros: &mut Vec<std::string::String>,
					state_do_while: &mut bool,
					planned_insertions: &mut Vec<Insertion>,
                    anal_opened_curl: Option<&Fn(anal_opened_curl_arguments)>,
					anal_function_body: Option<&Fn(anal_function_body_arguments)>,
					anal_closed_curl: Option<&Fn(anal_closed_curl_arguments)>,
					anal_command: Option<&Fn(anal_command_arguments)>) {
	if DEBUG_PRINTS >= 10 {
		println!("{}", format!("{:?}", state_curled_braces).replace("\n", "_"));
		print_state(&source, *posp, 0, "cycle begin");
	}
	check_and_jump_comment_and_whitespace(&source, posp);
	if DEBUG_PRINTS >= 10 {
		print_state(&source, *posp, 1, "after whitespaces and comments");
	}
	if *posp >= source.len() { //diese if-schleife vielleicht wegmachen und ganz unten statt else { else if pos < content_length hinschreiben
		return;
	}
	if check_and_jump_hashtag_command_otwc(source, posp, state_file_macros) {
	} else if str_at_pos(&source, *posp, "}") {
		if let Some(f) = anal_closed_curl {
			let a = anal_closed_curl_arguments{source: &source, pos: *posp, state_curled_braces: state_curled_braces};
			//f(anal_closed_curl_arguments{source: &source, pos: *posp, state_curled_braces: &mut state_curled_braces} );
		}
		state_curled_braces.pop();
		if DEBUG_PRINT_CURLED {
			println!("found a }}");
			println!("new state_curled_braces is {:?}", state_curled_braces);
		}
		*posp += 1;
	} else if (slice_until_delim_or_eof(source, *posp, &[":", " ", "\t", "\n", "/*", "//"]) == "public" ||
		slice_until_delim_or_eof(source, *posp, &[":", " ", "\t", "\n", "/*", "//"]) == "private" ||
		slice_until_delim_or_eof(source, *posp, &[":", " ", "\t", "\n", "/*", "//"]) == "protected") { //wenn man die äußeren klammern wegmacht, hat emacs problem emit dem einrücken
		*posp += 6;
		if !jump_to_normal(&source, posp, &[":"]) {
			panic!("unable to find matching : for public private or protected");
		}
		*posp += 1;
	} else if slice_until_delim_or_eof(source, *posp, &["(", " ", "\t", "\n", "/*", "//"]) == "for" { //TODO: anstelle das man hier for und hile schleifen hat, kann man auch sagen das man strichpunkte nur akzeptiert wenn diese nicht innerhalb von runden klammern sind
		expect_and_jump_braces(source, posp, "(", ")");
		let old_pos = *posp;
		if !jump_to_normal(&source, posp, &["{", ";"]) {
			panic!(format!("unable to find matching {{ or ; for for loop.\nFile: {}\nLine: {}", filepath, pos_to_line(source, *posp)));
		}
		if str_at_pos(source, *posp, "{") {
			state_curled_braces.push(&source[old_pos..*posp]);
		}
		*posp += 1;
	} else if slice_until_delim_or_eof(source, *posp, &["(", " ", "\t", "\n", "/*", "//"]) == "while" {
		if !*state_do_while {
			expect_and_jump_braces(source, posp, "(", ")");
			let old_pos = *posp;
			if !jump_to_normal(&source, posp, &["{", ";"]) {
				panic!(format!("unable to find matching {{ or ; for while loop.\nFile: {}\nLine: {}", filepath, pos_to_line(source, *posp)));
			}
			if str_at_pos(source, *posp, "{") {
				state_curled_braces.push(&source[old_pos..*posp]);
			}
			*posp += 1;
		} else {
			if !jump_to_normal(&source, posp, &[";"]) {
				panic!("unable to find matching ; for do-while loop");
			}
            *state_do_while = false;
			*posp += 1;
		}
	} else if slice_until_delim_or_eof(source, *posp, &["{", " ", "\t", "\n", "/*", "//"]) == "do" {
		*state_do_while = true;
		let old_pos = *posp;
		if !jump_to_normal(&source, posp, &["{"]) {
			panic!("unable to find matching { for do-while loop");
		}
		state_curled_braces.push(&source[old_pos..*posp]);
		*posp += 1;
	} else if check_and_jump_known_macro_otwc(source, posp, filepath, state_file_macros) {
	} else if str_at_pos(source, *posp, "direction correspondence[3]{0, 0, 0};") { //TODO was ist das? Das ist die initialisierung ein Array von Klassen
		*posp += "direction correspondence[3]{0, 0, 0};".len(); //src/OpenFOAM/primitives/triad/triad.C Zeile 211
	} else { //TODO geschweifte klammern, semikolons, /* und // in Anführungszeichen, mutli line comment in nem single line comment
		if DEBUG_PRINTS >= 10 { 
			println!("\tFound something else");
		}
		let old_pos = *posp;
		if !jump_to_normal(&source, posp, &[";", "}", "{", "#define"]) { //todo kan das "}" hier weg?
			panic!("unable to find ; or }} or {{");
		}
		if str_at_pos(&source, *posp, "#define") {
			panic!("suspicious thing in file {} line {}", filepath, pos_to_line(source, *posp));
		}
		if DEBUG_PRINTS >= 10 {
			print_state(source, *posp, 1, "Found a ; or }} or {{");
		}
		/*if DEBUG_PRINTS >= 10 {
		print_state(&source, pos, 1, "after searching for ; or {");
	}*/
		if str_at_pos(&source, *posp, "{") { //wenn es von einer { beendet wird muss alles bis zur } übersprungen werdne
			if DEBUG_PRINT_CURLED {
				print_state(source, *posp, 0, "Found a {{ at ")
			}
			if DEBUG_PRINTS >= 10 {
				println!("\tIt's a {{");
			}
			//&source[old_pos..pos] ist der name der funktion inklusive allem, bis zur {
			let mut pos_inside_function = false;
			for cb in state_curled_braces.iter() { //#PERFORMANCE: reason_for_curled_brace wird öfters für dasselbe argument berechnet
				if let ReasonCurledBrace::Function = reason_for_curled_brace(&cb){
					pos_inside_function = true;
				}
			}
			if !pos_inside_function { //Diese if schleife sollte iegentlich unnötig sein, aber evtl. verbessert sie die performance und schützt vor abstürzen falls reason_for_curled_brace buggy ist
				if let Some(f) = anal_opened_curl {
                    f(anal_opened_curl_arguments{source: &source, start_pos: old_pos, end_pos: *posp, file: filepath, reason: reason_for_curled_brace(&source[old_pos..*posp]), planned_insertions: planned_insertions});
                    //#PERFORMANCE: reason_for_curled_bracewitd öfters für dasselbe argument berechnet
				}
				if let ReasonCurledBrace::Function = reason_for_curled_brace(&source[old_pos..*posp]) {
					let mut curled_braces_depth = 1;
					let mut temp_pos = *posp;
					while curled_braces_depth != 0 {
						temp_pos += 1;
						if !jump_to_next_normal_slice_with_cond(&source, &mut temp_pos, 1, &|s| s == "}" || s == "{") {
							print_state(&source, *posp, 0, "unable to find }}");
							println!("Problem in file: {}", filepath);
							if DEBUG_PANIC_ON_PROBLEM {
								panic!("Parsing Problem");
							}
							//return;
							break;
						}
						//println!("FOUND A CHAR: {}", &source[pos..pos+1]);
						if &source[temp_pos..temp_pos+1] == "}" {
							curled_braces_depth -= 1;
							if DEBUG_PRINTS >= 20 {
								print_state(&source, temp_pos, 2, &format!("found a }}, new depth = {}", curled_braces_depth));
							}
						} else if &source[temp_pos..temp_pos+1] == "{" {
							curled_braces_depth += 1;
							if DEBUG_PRINTS >= 20 {
								print_state(&source, temp_pos, 2, &format!("found a {{, new depth = {}", curled_braces_depth));
							}
						}
					}
					if let Some(f) = anal_function_body {
						f(anal_function_body_arguments{source: source, start_pos: old_pos, middle_pos: *posp, end_pos: temp_pos+1, file: filepath, planned_insertions: planned_insertions});
					}
					/*if source[pos..temp_pos].contains("rho0") {
					print_state(source, pos, 1, "Inserting Hook");
					add_runpoint(&mut planned_insertions, pos+1, "rho0");
				}*/
				}
			}
			//println!("===========\n{}===========", &source[old_pos..pos]);
			state_curled_braces.push(&source[old_pos..*posp]);
			if DEBUG_PRINT_CURLED {
				println!("new state_curled_braces is {:?}", state_curled_braces);
			}
			//outfile_curl.write(&source[old_pos..pos].as_bytes()).expect("unable to write");
			//println!("{}", &source[old_pos..pos]);
			//println!("{}", &source[pos-300..pos+30]);
			//return;
			/*let mut curled_braces_depth = 1;
			while curled_braces_depth != 0 {
			pos += 1;
			if str_at_pos(&source, pos, "{") {
			curled_braces_depth += 1;
		} else if str_at_pos(&source, pos, "}") {
			curled_braces_depth -= 1;
		}
		}*/
		} else if str_at_pos(&source, *posp, ";") {
			if let Some(f) = anal_command {
				f(anal_command_arguments{source: source, start_pos: old_pos, end_pos: *posp, state_curled_braces: state_curled_braces, file: filepath, planned_insertions: planned_insertions});
			}
		}
		if !str_at_pos(&source, *posp, "}") {
			*posp += 1;
		}
		/*if DEBUG_PRINTS >= 10 {
		print_state(&source, pos, 1, "after block");
	}*/
	}
	if DEBUG_PAUSES >= 5 && pos_to_line(source, *posp) >= DEBUG_PAUSE_AFTER_LINE {
		pause();
	}
}
