use std::io::prelude::*; //TODO das soll irgenwannmal wegkommen
#[macro_use]
extern crate lazy_static;
extern crate regex;
extern crate walkdir;

mod test;
mod misc;
use misc::*;
mod stranal;
use stranal::*;
mod helper;
use helper::*;
mod simplecpp;
use simplecpp::*;
mod lex_step;
use lex_step::*;
mod lex_path;
use lex_path::*;

/*fn anal_function_body_grep(search: &str, msg: &'static str, source: &str, start_pos:usize, middle_pos:usize, end_pos: usize, planned_insertions: &mut Vec<Insertion>) { //TODO lifetimes auf die reihe kreigen, planned_insertions soll die ownership der Strings haben
	if source[start_pos..end_pos].contains(search) {
		add_runpoint(planned_insertions, middle_pos+1, msg);
	}
}
//Diese Funktion ist nur dazu da um zu überprüfen ob die Hooks funktionieren
fn anal_function_body_local(source: &str, start_pos:usize, middle_pos:usize, end_pos: usize, planned_insertions: &mut Vec<Insertion>) {
	if source[start_pos..end_pos].contains("known patch interaction ty") {
		add_runpoint(planned_insertions, middle_pos+1, "known patch interaction ty");
	}
}
fn anal_function_body(source: &str, start_pos:usize, middle_pos:usize, end_pos: usize, planned_insertions: &mut Vec<Insertion>) {
	anal_function_body_grep("\"rho0\"", "srho0", source, start_pos, middle_pos, end_pos, planned_insertions);
	anal_function_body_grep("\"T0\"", "sT0", source, start_pos, middle_pos, end_pos, planned_insertions);
	anal_function_body_grep("known patch interaction ty", "local test", source, start_pos, middle_pos, end_pos, planned_insertions);
}
fn anal_command_correct(source: &str, start_pos: usize, end_pos: usize, file: &str) {
	let mut pos = start_pos;
	loop {
		let f = &source[pos..end_pos].find("correct");
		if let Some(x) = f {
			let mut temp_pos = pos+x+"correct".len();
			pos += x+1;
			if !str_white_at_pos(&source, temp_pos) && !str_at_pos(&source, temp_pos, "(") {
				continue;
			}
			if !jump_to_next_normal_slice_with_cond(&source, &mut temp_pos, 1, &|s| s == "(") {
				return;
			}
			temp_pos += 1;
			let mut pos_after_brace = temp_pos;
			let mut num_commas = 0;
			let mut braces_depth = 1;
			while braces_depth != 0 {
				if !jump_to_next_normal_slice_with_cond(&source, &mut temp_pos, 1, &|s| s == ")" || s == "(" || s == "," ) {
					num_commas = -2;
					break;
				}
				//println!("vor den checks {} {} {}", braces_depth, temp_pos, &source[temp_pos..temp_pos+1]);
				if &source[temp_pos..temp_pos+1] == ")" {
					braces_depth -= 1; 
				} else if &source[temp_pos..temp_pos+1] == "(" {
					braces_depth += 1;
				} else if &source[temp_pos..temp_pos+1] == "," && braces_depth == 1{
					num_commas += 1;
				}
				temp_pos += 1;
				//println!("nach den checks {} {} {}", braces_depth,  temp_pos, &source[temp_pos..temp_pos+1]);
			}
			let mut num_arg;
			if num_commas == 0 {
				if !jump_to_next_normal_slice_with_cond(&source, &mut pos_after_brace, 1, &|s| !(s.trim().is_empty())) {
					num_arg = -1;
				} else if &source[pos_after_brace..pos_after_brace+1] == ")" {
					num_arg = 0;
				} else {
					num_arg = 1;
				}
			} else {
				num_arg = num_commas + 1;
			}
			if num_arg == 3 {
                println!("{:?} {} {} {}", file, pos_to_line(&source, start_pos), num_arg, &source[start_pos..end_pos]);
			}
		} else {
			return;
		}
	}
}
fn anal_function_body_test(source: &str, start_pos:usize, middle_pos:usize, end_pos: usize, planned_insertions: &mut Vec<::Insertion>) {
	//println!("{}", &source[start_pos..middle_pos]);
	//print_state(source, start_pos, 0, "func signaturef");
	anal_function_signature(&source[start_pos..middle_pos]);
}
fn anal_closed_curl_test(source: &str, pos: usize, state_curled_braces: &Vec<&str>) {
	println!("{:?}", state_curled_braces);
}*/
//static HOOK_INCLUDES_FOR_RUNPOINTS: &'static str = "/*BEGIN_LEXA*/#include<fstream>\n#include<chrono>/*END_LEXA*/\n";
//static HOOK_BEGIN_RUNPOINT: &'static str = "/*BEGIN_LEXA*/{std::ofstream LEXA_OFS(\"/opt/HOOK_OUT.txt\",std::ofstream::out|std::ofstream::app);LEXA_OFS<<std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count()<<' '<<__FILE__<<' '<<__func__<<' '<<__LINE__<<\": ";
//static HOOK_END_RUNPOINT: &'static str = "\" << '\\n';}/*END_LEXA*/\n";
//	planned_insertions.push(Insertion {pos: pos, txt: &HOOK_BEGIN_RUNPOINT});
static mut POS_OF_FUNC: usize = 0;
fn hook_every_function_start_and_begin(_source: &str, _start_pos:usize, middle_pos:usize, end_pos: usize, file: &str, planned_insertions: &mut Vec<::Insertion>) {
	//print_state(_source, _start_pos, 0, "hook_funktion");
	unsafe {
		POS_OF_FUNC = middle_pos;
	}
	planned_insertions.push(Insertion {pos: 0, txt: "/*BEGIN_LEXA*/#include<fstream>/*END_LEXA*/\n".to_string()});
	//let rp = "/*BEGIN_LEXA*/{std::ofstream LEXA_OFS(\"/opt/HOOK_OUT.txt\",std::ofstream::out|std::ofstream::app);LEXA_OFS<<'B'<<".to_string()
	//	+ file.to_string()  + "<<'|'<<".to_string() + middle_pos.to_string()
	//	+ ";}/*END_LEXA*/\n".to_string();
	//man beachte, dass sich rpB und rpE in einem LEXA_OFS<<'B' zu LEXA_OFS<<'E' unterscheiden
	let rp_b = format!("/*BEGIN_LEXA*/{{std::ofstream LEXA_OFS(\"/opt/HOOK_OUT.txt\",std::ofstream::out|std::ofstream::app);LEXA_OFS<<'B'<<\"{}\"<<'|'<<{}<<'\\n';}}/*END_LEXA*/\n", file, middle_pos);
	planned_insertions.push(Insertion {pos: middle_pos+1, txt: rp_b});
	let rp_e = format!("/*BEGIN_LEXA*/{{std::ofstream LEXA_OFS(\"/opt/HOOK_OUT.txt\",std::ofstream::out|std::ofstream::app);LEXA_OFS<<'E'<<\"{}\"<<'|'<<{}<<'\\n';}}/*END_LEXA*/\n", file, middle_pos);
	planned_insertions.push(Insertion {pos: end_pos-1, txt: rp_e});
}
//#![allow(unused_variable)]
fn hook_returns(source: &str, start_pos: usize, _end_pos: usize, _state_curled_braces: &Vec<&str>, file: &str, planned_insertions: &mut Vec<::Insertion>) {
	if slice_until_delim_or_eof(source, start_pos, &[";", " ", "\t", "\n", "//", "/*"]) == "return" {
		unsafe {
			let rp_e = format!("/*BEGIN_LEXA*/{{std::ofstream LEXA_OFS(\"/opt/HOOK_OUT.txt\",std::ofstream::out|std::ofstream::app);LEXA_OFS<<'E'<<\"{}\"<<'|'<<{}<<'\\n';}}/*END_LEXA*/\n", file, POS_OF_FUNC);
			planned_insertions.push(Insertion {pos: start_pos, txt: rp_e});
		}
		//add_runpoint(planned_insertions, start_pos, "exit");
	}
}
fn hook_Cp0(source: &str, start_pos:usize, middle_pos:usize, end_pos: usize, file: &str, planned_insertions: &mut Vec<::Insertion>) {
	if source[start_pos..end_pos].contains("\"Cp0\"") {
		let incl = format!("/*BEGIN_LEXA*/#include<fstream>/*END_LEXA*/\n");
		planned_insertions.push(Insertion {pos: 0, txt: incl});
		let rp = format!("/*BEGIN_LEXA*/{{std::ofstream LEXA_OFS(\"/opt/HOOK_OUT.txt\",std::ofstream::out|std::ofstream::app);LEXA_OFS<<__FILE__<<':'<<__LINE__<<'\\n';}}/*END_LEXA*/\n");
		planned_insertions.push(Insertion {pos: middle_pos+1, txt: rp});
	}
}
static INSERTSTR_INCLUDE_FSTREAM: &str = "/*BEGIN_LEXA*/#include<fstream>/*END_LEXA*/\n";
static INSERTSTR_RUNPOINT: &str = "/*BEGIN_LEXA*/{{std::ofstream LEXA_OFS(\"/opt/HOOK_OUT.txt\",std::ofstream::out|std::ofstream::app);LEXA_OFS<<__FILE__<<':'<<__LINE__<<'\\n';}}/*END_LEXA*/\n";

fn anal_opened_curl_string(search: &str, source: &str, start_pos: usize, end_pos: usize, file: &str, reason: ReasonCurledBrace, planned_insertions: &mut Vec<::Insertion>) {
	if let ReasonCurledBrace::Function = reason {
		if source[start_pos..end_pos].contains(search) {
			//println!("{}", &source[start_pos..end_pos]);
			//println!("{}", &source[end_pos..end_pos+1]);
			let incl = String::from(INSERTSTR_INCLUDE_FSTREAM);
			planned_insertions.push(Insertion {pos: 0, txt: incl});
			let rp = String::from(INSERTSTR_RUNPOINT);
			planned_insertions.push(Insertion {pos: end_pos+1, txt: rp});
		}
	}
}
fn anal_every_command_string(search: &str, source: &str, start_pos: usize, end_pos: usize, state_curled_braces: &Vec<&str>, file: &str, planned_insertions: &mut Vec<::Insertion>) {
	if source[start_pos..end_pos].contains(search) {
		let incl = String::from(INSERTSTR_INCLUDE_FSTREAM);
		planned_insertions.push(Insertion {pos: 0, txt: incl});
		let rp = String::from(INSERTSTR_RUNPOINT);
		planned_insertions.push(Insertion {pos: start_pos, txt: rp});
		let rp = String::from(INSERTSTR_RUNPOINT);
		planned_insertions.push(Insertion {pos: end_pos+1, txt: rp});
		//println!("{}", &source[start_pos..end_pos]);
	}
}
fn anal_opened_curl(source: &str, start_pos: usize, end_pos: usize, file: &str, reason: ReasonCurledBrace, planned_insertions: &mut Vec<::Insertion>) {
	anal_opened_curl_string("rho_", source,start_pos,end_pos,file,reason,planned_insertions);
	//anal_opened_curl_string("rho0_", source,start_pos,end_pos,file,reason,planned_insertions);
}
fn anal_every_command(source: &str, start_pos: usize, end_pos: usize, state_curled_braces: &Vec<&str>, file: &str, planned_insertions: &mut Vec<::Insertion>) {
	anal_every_command_string("rho_", source,start_pos,end_pos,state_curled_braces,file,planned_insertions);
	anal_every_command_string("rho0_", source,start_pos,end_pos,state_curled_braces,file,planned_insertions);
}
//use as anal_opened_curl argument
//fn class_search(source: &str, start_pos: usize, file: &str, reason: ReasonCurledBrace) {
fn class_search(arg: anal_opened_curl_arguments) {
	if let ReasonCurledBrace::Class(name) = arg.reason {
        //println!("{}", &arg.source[arg.start_pos..arg.end_pos]);
		let mut classfile = std::fs::OpenOptions::new().write(true).append(true).open("classes.txt").unwrap();
		classfile.write_all(&format!("{} {} {}\n", name, arg.file, pos_to_line(arg.source, arg.start_pos)).into_bytes()).expect("unable to write into classfile");
	}
}
enum EnumTest <'a> {
	Test (&'a str),
    TestB
}
fn main() {
    /*let a;
    {
        let src = std::fs::read_to_string("/home/volker/test.txt").expect("unable to read file");
				//let source = source.replace(|c: char| !c.is_ascii(), ""); //#PERFORMANCE
		let src = &src;
        a = EnumTest::Test(&src[1..4]);
    }
    if let EnumTest::Test(name) = a {
        println!("{}", name);
    }
    return;*/
    
    lex_path(std::path::Path::new("/home/volker/SIMTAP_DIR/openfoam-opt/src"), false, Some(&class_search), None, None, None);
	//remove_hooks(std::path::Path::new("/opt/srconly/src"));
	//lex_path(std::path::Path::new("/opt/srconly/src/lagrangian/intermediate/parcels/Templates/KinematicParcel"), true, Some(&anal_opened_curl), None, None, Some(&search_rho));
	return;
    
	/*println!("Are you sure, that you want to write into the files ? Enter new to remove old hooks and add new hooks:");
	let mut buffer = String::new();
	std::io::stdin().read_line(&mut buffer).expect("unable to read from stdin");
	let mut write_flag = false;
	if buffer == "new\n" {
		remove_hooks(std::path::Path::new("/opt/openfoam6/src"));
		write_flag = true;
	}*/
}
fn bench_walker() {
	let mut num = 0;
	for entry in walkdir::WalkDir::new(std::path::Path::new("/opt/openfoam6/src/")) {
		let entry = entry.expect("weird error");
		if entry.file_type().is_file() {
			let extension = match entry.path().extension() {
				Some(ext) => ext.to_str().expect("weird error"),
				None => "",
			};
			if extension == "C" || extension == "H" {
				let content = std::fs::read_to_string(entry.path()).expect("unable to read file");
				let content_length = content.len();
				let source = content.as_str();
				for i in 0..1 {
					let mut pos = 0;
					/*let anal_closure = |src: &str, pos: usize, n: &mut i32| -> bool { //10x: 6,52 s 1x: 2,7
						if str_at_pos_half_check(src, pos, "(") {
							*n += 1;
						} else if str_at_pos_half_check(src, pos, ")") {
							*n -= 1;
						}
						return false;
					};
					walk_normal_str_with_closure(source, &mut pos, anal_closure, &mut num);*/
					/*while pos < content_length { //10x 7,96 s 1x: 2,7
						check_and_jump_comment_and_whitespace(source, &mut pos);
						if str_at_pos(source, pos, "(") {
							num += 1;
						}
						if str_at_pos(source, pos, ")") {
							num -= 1;
						}
						pos += 1;
					}*/
					/*loop {
						if !jump_to_normal(source, &mut pos, &["(", ")"]) { //8,03 s
							break;
						}
						if str_at_pos(source, pos, "(") {
							num += 1;
						} else {
							num -= 1;
						}
						pos += 1;
					}*/
					
					/*while pos < content_length { //3,03 s
						if str_at_pos(source, pos, "(") {
							num += 1;
						}
						if str_at_pos(source, pos, ")") {
							num -= 1;
						}
						pos += 1;
					}*/
				}
			}
		}
    }
	println!("num = {}", num);
}
