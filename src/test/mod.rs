#[cfg(test)]
//mod test {
use std; //https://stackoverflow.com/questions/23434378/use-of-undeclared-type-or-module-std-when-used-in-a-separate-module
#[test]
fn remove_hooks() {
	std::fs::copy("testfiles/tc_remove.C", "testfiles/tt_remove.C").expect("unable to copy file");
	::remove_hooks(std::path::Path::new("testfiles/tt_remove.C"));
	let answer = std::fs::read_to_string("testfiles/tt_remove.C").expect("unable to read file");
	let solution = std::fs::read_to_string("testfiles/ts_remove.C").expect("unable to read file");
	//std::fs::remove_file("testfiles/tt_remove.C").expect("unable to remove file");
	assert!(answer == solution);
}
#[test]
fn lex_without_crash() {
	::lex_path(std::path::Path::new(&(std::env::var("WM_PROJECT_DIR").expect("$WM_PROJECT_DIR not set")+"/src")), false, None, None, None, None);
}
/*fn anal_function_body_helper(source: &str, start_pos:usize, middle_pos:usize, end_pos: usize, _file: &str, _planned_insertions: &mut Vec<::Insertion>) {

	/*println!("{}", &source[start_pos..middle_pos]);
	println!("{}", &source[middle_pos..end_pos]);*/
	assert!(&source[middle_pos..middle_pos+1]=="{");
	assert!(&source[end_pos-1..end_pos]=="}");
	if source[start_pos..end_pos].contains("b") {
		::add_runpoint(planned_insertions, middle_pos+1, "toller test"); TODO seit der änderung von &str zu String in Insertion funtkioniert das nemme
	}
}
#[test]
fn anal_function_body_compiles() {
	std::fs::copy("testfiles/tc_function.C", "testfiles/tt_function.C").expect("unable to copy file");
	::lex_path(std::path::Path::new("testfiles/tt_function.C"), true, Some(&anal_function_body_helper), None, None);
	let status = std::process::Command::new("g++")
		.arg("-std=c++11")
		.arg("testfiles/tt_function.C")
		.arg("-o")
		.arg("/dev/null")
		.status()
		.expect("failed to execute process");
	assert!(status.success());
}*/
#[test]
fn jtnnswc1() {
	let source = "FatalIOErrorInFunction(is)  << \"incorrect list token, expected '(' or '{', found \"  << firstTok.info()  << exit(FatalIOError);";
	//let source = "abc\"123\"def";
	let mut pos = 0;
	assert!(!::jump_to_next_normal_slice_with_cond(&source, &mut pos, 1, &|s| s == "}" || s == "{"));
}
#[test]
fn jtnnswc2() {
	let source = "//abc\n}";
	//let source = "abc\"123\"def";
	let mut pos = 0;
	assert!(::jump_to_next_normal_slice_with_cond(&source, &mut pos, 1, &|s| s == "}" || s == "{"));
}
#[test]
fn jtnnswc3() {
	let source = "//abc}\ndef";
	//let source = "abc\"123\"def";
	let mut pos = 0;
	assert!(!::jump_to_next_normal_slice_with_cond(&source, &mut pos, 1, &|s| s == "}" || s == "{"));
}
#[test]
fn jtnnswc4() {
	let source = r#"//    dict.readIfPresent("zone", zoneKey_);
//
//    if (debug && zoneKey_.size() && mesh.cellZones().findZoneID(zoneKey_) < 0)
//    {
//        Info<< "cellZone " << zoneKey_
//            << " not found - using entire mesh" << endl;
//    }
}"#;
	let mut pos = 0;
	assert!(::jump_to_next_normal_slice_with_cond(&source, &mut pos, 1, &|s| s == "}" || s == "{"));
	::print_state(&source, pos, 0, "test");
	println!("pos = {}", pos);


}
#[test]
fn jtnnswc5() {
	let source = "/*a*//*b*/";
	let mut pos = 0;
	assert!(!::jump_to_next_normal_slice_with_cond(&source, &mut pos, 1, &|s| s == "b"));

	let source = "//a\n//b";
	let mut pos = 0;
	assert!(!::jump_to_next_normal_slice_with_cond(&source, &mut pos, 1, &|s| s == "b"));
}
#[test]
fn jtnnswc_close_after() {
	let source = "/*123*/a\"123\"b//123\nc";
	let mut pos = 0;
	assert!(::jump_to_next_normal_slice_with_cond(&source, &mut pos, 1, &|s| s == "a"));
	assert!(::jump_to_next_normal_slice_with_cond(&source, &mut pos, 1, &|s| s == "b"));
	assert!(::jump_to_next_normal_slice_with_cond(&source, &mut pos, 1, &|s| s == "c"));
}
/*#[test]
fn reason_for_curled_brace() {
	let pairs = [
		(&"namespace Foam",::simplecpp::OBJ_NAMESPACE),
		(&"class test",::simplecpp::OBJ_CLASS),
		(&"template<class ParcelType> class KinematicParcel : public ParcelType",::simplecpp::OBJ_CLASS),
		(&"int foo [5] = ", ::simplecpp::OBJ_ARRAY_DEF),
		(&"Foam::pointMVCWeight::pointMVCWeight( const int a, const int b) : cellIndex_((cellIndex != -1) ? cellIndex : mesh.faceOwner()[faceIndex])", ::simplecpp::OBJ_FUNCTION)
	];
	for p in pairs.iter() {
		assert!(::simplecpp::reason_for_curled_brace(p.0) == p.1);
	}
	//let questions = &["namespace Foam", "", ""];
	//let solutions = [::simplecpp::OBJ_NAMESPACE, ::simplecpp::OBJ_CLASS, ::simplecpp::OBJ_CLASS];
	//for i in 0..questions.len() {
	//	assert!(::simplecpp::reason_for_curled_brace(questions[i]) == solutions[i]);
	//}
}*/
#[test]
fn anal_function_signature() {
	let src = r#"template<class ParcelType>
Foam::KinematicParcel<ParcelType>::KinematicParcel
(
    const KinematicParcel<ParcelType>& p
)
:
    ParcelType(p),
    active_(p.active_),
    typeId_(p.typeId_),
    nParticle_(p.nParticle_),
    d_(p.d_),
    dTarget_(p.dTarget_),
    U_(p.U_),
    rho_(p.rho_),
    age_(p.age_),
    tTurb_(p.tTurb_),
    UTurb_(p.UTurb_)"#;
	::simplecpp::anal_function_signature(src);
	//let questions = &["template<class ParcelType>template<class TrackCloudType>void Foam::KinematicParcel<ParcelType>::setCellValues(TrackCloudType& cloud, trackingData& td)"];
	//for i in 0..questions.len() {
	//	::simplecpp::anal_function_signature(questions[i]);
	//}
}
	/*PERFORMANCE TEST
let source = "abcdefghijklmnopqrstuvwxyzß.äüö".repeat(10000000);
	println!("{}", source.len());
	let mut count = 0;
	let source2 = source.as_str();
	for i in 0..source2.len() {
		if ::str_at_pos(source2, i, "p") {
			count += 1;
		}
	}
	let mut iter = source.chars();
	while let Some(c) = iter.next() {
		if c == 'p' {
			count += 1;
		}
	}
	println!("{}", count);
	return;*/
/*#[test]
fn var2_anal_str() {
	let source = "abcdefghijklmnopqrstuvwxyzß.äüö".repeat(10000000);
	println!("length = {}", source.len());
	let source = source.as_str();
	let anal_closure = |src: &str, pos: usize, count: i32| -> i32{
		if ::str_at_pos(src, pos, "f") /*|| ::str_at_pos(src, pos, "g")*/ {
			return count + 1;
		}
		return count;
	};
	println!("returning = {}", ::stranal::var2_anal_str_with_closure(source, anal_closure, 0));
}*/
/*#[test]
fn anal_str() {
	let source = "abcdefghijklmnopqrstuvwxyzß.äüö".repeat(10000000);
	println!("length = {}", source.len());
	let source = source.as_str();
	let anal_closure = |src: &str, pos: usize, count: &mut i32| {
		if ::str_at_pos(src, pos, "f") /*|| ::str_at_pos(src, pos, "g")*/ {
			*count += 1;
		}
	};
	let mut count = 0;
	::stranal::anal_str_with_closure(source, anal_closure, &mut count);
	println!("count = {}", count);
}*/
//}
