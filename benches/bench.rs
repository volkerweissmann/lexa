#[macro_use]
extern crate criterion;
extern crate walkdir;
use criterion::Criterion;
fn lex() {
	for entry in walkdir::WalkDir::new(std::path::Path::new("/opt/openfoam6/src/")) {
		let entry = entry.expect("weird error");
		if entry.file_type().is_file() {
			let extension = match entry.path().extension() {
				Some(ext) => ext.to_str().expect("weird error"),
				None => "",
			};
			if extension == "C" || extension == "H" {
				let content = std::fs::read_to_string(entry.path()).expect("unable to read file");
			}
		}
    }
}
fn helper(c: &mut Criterion) {
	//c;
	/*let c = c.into_inner();
	let c = c.sample_size(3);*/
	c.sample_size(3).bench_function("fib 20", |b| b.iter( || lex() ));
}
criterion_group!(benches, helper);
criterion_main!(benches);
